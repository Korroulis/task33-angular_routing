import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  save(session:any) {
    localStorage.setItem('session',JSON.stringify(session));
  }


  get(): any {
    const savedSession = localStorage.getItem('session');
    return savedSession ? JSON.parse(savedSession) : false;
  }

  remove(): any {
    localStorage.removeItem('session');
  }

}
