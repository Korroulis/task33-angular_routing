import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SessionService } from '../session/session.service';

@Injectable({
  providedIn: 'root'
})
export class SurveyServiceService {

  constructor(private http:HttpClient, private session: SessionService) { }

  getSurveys(): Promise<any> {
    return this.http.get('https://survey-poodle.herokuapp.com/v1/api/surveys', {
      headers: {
        'Authorization': 'Bearer ' + this.session.get().token
      }
    }).toPromise();
  }

  getSurvey(id): Promise<any> {
    return this.http.get(`https://survey-poodle.herokuapp.com/v1/api/surveys/${id}`).toPromise();
  }

}

