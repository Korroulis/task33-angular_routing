import { Component, OnInit } from '@angular/core';
import {SurveyServiceService} from 'src/app/services/SurveyService/survey-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {

  surveys: any[]=[];

  constructor(private router: Router, private surveyService:SurveyServiceService) { }

  async ngOnInit() {

    try {
      const result: any = await this.surveyService.getSurveys();
      this.surveys=result.data;
      console.log('RESULT',result);

    } catch (e) {

    }
  }

  /* async load() {
  
    try {
      const result: any = await this.surveyService.getSurveys();
            console.log(result);

    } catch (e) {

    }
  }; */
  
}
