import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import {SessionService} from 'src/app/services/session/session.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public dashboardTitle = "Welcome to the dashboard!!!";

  constructor(private authService: AuthService, private router: Router, private session:SessionService) { }

  ngOnInit(): void {
  }

  get username() {
    return this.session.get().username;
  }
 /*  get surveys() {
    return
  } */

  get isLoggedIn() {
    console.log(this.authService.isLoggedIn());
    return this.authService.isLoggedIn();
  }

  logoutClick() {
    this.session.remove();
    this.router.navigateByUrl('/login')
  }

}
